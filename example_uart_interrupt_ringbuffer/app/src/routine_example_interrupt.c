/*
 * Copyright (c) 2022 SEBASTIAN BEDIN <sebabedin@gmail.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * @file   : routine_example_polling.c
 * @date   : May 18, 2022
 * @author : SEBASTIAN BEDIN <sebabedin@gmail.com>
 * @version	v1.0.0
 */

/********************** inclusions *******************************************/

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

#include <arch.h>
#include <config.h>

/********************** macros and definitions *******************************/

#define LPC_UART_                       (LPC_USART2)
#define LPC_UART_IRQ_                   (USART2_IRQn)
#define LPC_UART_IRQHANDLER_            (UART2_IRQHandler)
#define SCU_TX_PORT_                    (7)
#define SCU_TX_PIN_                     (1)
#define SCU_TX_FUNC_                    (FUNC6)
#define SCU_RX_PORT_                    (7)
#define SCU_RX_PIN_                     (2)
#define SCU_RX_FUNC_                    (FUNC6)

#define PERIOD_ticks_                   (100 / APP_CONFIG_PERIOD_ms)
#define DELAY_ticks_                    (500 / APP_CONFIG_PERIOD_ms)

#define BUFFER_SIZE_                    (64)

/********************** internal data declaration ****************************/

/********************** internal functions declaration ***********************/

/********************** internal data definition *****************************/

static RINGBUFF_T tx_ring_buffer_;
static uint8_t tx_buffer_[BUFFER_SIZE_];

static RINGBUFF_T rx_ring_buffer_;
static uint8_t rx_buffer_[BUFFER_SIZE_];

static uint8_t user_buffer_[BUFFER_SIZE_];

/********************** external data definition *****************************/

/********************** internal functions definition ************************/

//void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart)
//{
//  if(&huart3 == huart)
//  {
//    // RX
//    {
//      HAL_StatusTypeDef rx_status;
//      rx_status = HAL_UARTEx_ReceiveToIdle_IT(&huart3, buffer_, BUFFER_SIZE_);
//      if(HAL_OK != rx_status)
//      {
//        // error
//      }
//    }
//  }
//}

//void HAL_UARTEx_RxEventCallback(UART_HandleTypeDef *huart, uint16_t Size)
//{
//  if(&huart3 == huart)
//  {
//    // TX
//    {
//      Chip_UART_SendRB(LPC_UART_, &txring, pDat, tmp);
//
//
//      HAL_StatusTypeDef tx_status;
//      tx_status = HAL_UART_Transmit_IT(&huart3, buffer_, Size);
//      if(HAL_OK != tx_status)
//      {
//        // error
//      }
//    }
//  }
//}

void LPC_UART_IRQHANDLER_(void)
{
  Chip_UART_IRQRBHandler(LPC_UART_, &rx_ring_buffer_, &tx_ring_buffer_);
}


static void init_(void *parameters)
{
  Chip_UART_Init(LPC_UART_);
  Chip_UART_SetBaud(LPC_UART_, 115200);
  Chip_UART_ConfigData(LPC_UART_, (UART_LCR_WLEN8 | UART_LCR_SBS_1BIT));
  Chip_UART_SetupFIFOS(LPC_UART_, UART_FCR_FIFO_EN | UART_FCR_TX_RS | UART_FCR_RX_RS | UART_FCR_TRG_LEV3 );
  Chip_UART_TXEnable(LPC_UART_);

  Chip_SCU_PinMux(SCU_TX_PORT_, SCU_TX_PIN_, MD_PDN, SCU_TX_FUNC_);
  Chip_SCU_PinMux(SCU_RX_PORT_, SCU_RX_PIN_, MD_PLN | MD_EZI | MD_ZI, SCU_RX_FUNC_);

//  Chip_UART_ReadByte(LPC_UART_);

  RingBuffer_Init(&tx_ring_buffer_, tx_buffer_, 1, BUFFER_SIZE_);
  RingBuffer_Init(&rx_ring_buffer_, rx_buffer_, 1, BUFFER_SIZE_);

  Chip_UART_IntEnable(LPC_UART_, (UART_IER_RBRINT | UART_IER_RLSINT));

  NVIC_SetPriority(LPC_UART_IRQ_, 1);
  NVIC_EnableIRQ(LPC_UART_IRQ_);
}

static void loop_(void *parameters)
{
  int rx_len = 0;
  // RX
  {
    rx_len = Chip_UART_ReadRB(LPC_UART_, &rx_ring_buffer_, user_buffer_, (BUFFER_SIZE_ >> 2));
  }

  // TX
  {
    if(0 < rx_len)
    {
      Chip_UART_SendRB(LPC_UART_, &tx_ring_buffer_, user_buffer_, rx_len);
    }
  }

  return;
}

/********************** external functions definition ************************/

void routine_example_interrupt(void *parameters)
{
    static bool _init = true;
    static uint32_t ticks = DELAY_ticks_;
    if(_init)
    {
      _init = false;
      init_(parameters);
    }

    if(0 == ticks)
    {
      ticks = (0 < PERIOD_ticks_) ? PERIOD_ticks_ : 1;
      loop_(parameters);
    }
    ticks--;
}

/********************** end of file ******************************************/
