/*
 * Copyright (c) 2022 SEBASTIAN BEDIN <sebabedin@gmail.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * @file   : task_blinking.c
 * @date   : May 17, 2022
 * @author : SEBASTIAN BEDIN <sebabedin@gmail.com>
 * @version	v1.0.0
 */

/********************** inclusions *******************************************/

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

#include "arch.h"
#include "statemachine.h"

/********************** macros and definitions *******************************/

enum
{
  STATE_COLOR_1,
  STATE_COLOR_2,
  STATE__N,
};

/********************** internal data declaration ****************************/

/********************** internal functions declaration ***********************/

/********************** internal data definition *****************************/

static statemachine_state_t states_[STATE__N];
static statemachine_t sm_;
static TickType_t xLastWakeTime_;

/********************** external data definition *****************************/

/********************** internal functions definition ************************/

static void state_color_1_entry(statemachine_t *sm)
{
  gpio_driver_write(GPIO_DRIVER_ID_LED1, true);
}

static void state_color_1_loop(statemachine_t *sm)
{
  TickType_t xFrequency = 500;
  vTaskDelayUntil(&xLastWakeTime_, xFrequency);
  statemachine_go_to_state(sm, STATE_COLOR_2);
}

static void state_color_1_exit(statemachine_t *sm)
{
  gpio_driver_write(GPIO_DRIVER_ID_LED1, false);
}

static void state_color_2_entry(statemachine_t *sm)
{
  gpio_driver_write(GPIO_DRIVER_ID_LED2, true);
}

static void state_color_2_loop(statemachine_t *sm)
{
  TickType_t xFrequency = 1000;
  vTaskDelayUntil(&xLastWakeTime_, xFrequency);
  statemachine_go_to_state(sm, STATE_COLOR_1);
}

static void state_color_2_exit(statemachine_t *sm)
{
  gpio_driver_write(GPIO_DRIVER_ID_LED2, false);
}

/********************** external functions definition ************************/

void task_blinking(void *pvParameters)
{
  statemachine_init(&sm_, states_, STATE__N);
  statemachine_state_init(&sm_, STATE_COLOR_1, state_color_1_entry, state_color_1_loop, state_color_1_exit);
  statemachine_state_init(&sm_, STATE_COLOR_2, state_color_2_entry, state_color_2_loop, state_color_2_exit);

  gpio_driver_write(GPIO_DRIVER_ID_LED1, false);
  gpio_driver_write(GPIO_DRIVER_ID_LED2, false);

  xLastWakeTime_ = xTaskGetTickCount();
  while (true)
  {
    statemachine_loop(&sm_);
  }
}

/********************** end of file ******************************************/
