/*
 * packet_camelcase.h
 *
 *  Created on: May 31, 2022
 *      Author: seb
 */

#ifndef INC_PACKET_CAMELCASE_H_
#define INC_PACKET_CAMELCASE_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "stdint.h"
#include "string.h"
#include "packet_define.h"

#ifdef __cplusplus
}
#endif

#endif /* INC_PACKET_CAMELCASE_H_ */
