/*
 * packet.cpp
 *
 *  Created on: May 27, 2022
 *      Author: seb
 */

#ifdef __cplusplus
extern "C" {
#endif

#include "stdint.h"
#include "string.h"
#include "packet_private.h"

static void error_message_generate(packet_message_t *message, message_errorcode_t error_code)
{
  message->buffer[0] = 'E';
  message->buffer[1] = '0';
  message->buffer[2] = '0' + error_code;
  message->size = 3;
}

static bool check_action_(packet_message_t *message)
{
  packet_byte_t action = message->buffer[0];
  if ((ACTION_CAMELCASE_ == action) || (ACTION_PASCALCASE_ == action) || (ACTION_SNAKECASE_ == action))
  {
    return true;
  }
  return false;
}

static bool packet_action_error_(void)
{
  return false;
}

static bool action_(packet_message_t *self)
{
  packet_byte_t action = self->buffer[0];
  switch (action)
  {
    case ACTION_CAMELCASE_:
    {
      packet_camelcase_t *camelcase = packet_camelcase_init_(self);
      return packet_camelcase_process_(camelcase);
    }
      break;
    case ACTION_PASCALCASE_:
      return packet_action_error_();
      break;
    case ACTION_SNAKECASE_:
      return packet_action_error_();
      break;
    default:
      return packet_action_error_();
      break;
  }
}

// ----------------------------------------------------------------------------

packet_message_t *packet_message_init_(packet_protocol_t *protocol)
{
  packet_message_t *self = &(protocol->message);
  self->protocol = protocol;
  self->buffer = protocol->buffer + 5;
  self->size = protocol->size - PACKET_PROTOCOL_MIN_SIZE;

  packet_datachecked_init_(self);

  return self;
}

void packet_message_process(packet_message_t *message)
{
  if (false == check_action_(message))
  {
    error_message_generate(message, MESSAGE_ERRORCODE_INVALID_OPCODE);
    return;
  }

  if(false == packet_datachecked_check_(&(message->datachecked)))
  {
    error_message_generate(message, MESSAGE_ERRORCODE_INVALID_DATA);
  }

  if (false == action_(message))
  {
    error_message_generate(message, MESSAGE_ERRORCODE_INVALID_DATA);
  }

  return;
}

//void packet_message_create(packet_message_t *self, packet_protocol_t *protocol, packet_byte_t *buffer, size_t size)
//{
//  self->protocol = protocol;
//  self->buffer
//}

#ifdef __cplusplus
}
#endif
