/*
 * Copyright (c) 2022 SEBASTIAN BEDIN <sebabedin@gmail.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * @file   : uart_dma_driver.c
 * @date   : May 26, 2022
 * @author : SEBASTIAN BEDIN <sebabedin@gmail.com>
 * @version	v1.0.0
 */

/********************** inclusions *******************************************/

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#include <arch.h>
#include <config.h>

#include <uart_dma_driver.h>

/********************** macros and definitions *******************************/

#define LPC_UART_                       (LPC_USART2)
#define LPC_UART_IRQ_                   (USART2_IRQn)
#define LPC_UART_IRQHANDLER_            (UART2_IRQHandler)
#define SCU_TX_PORT_                    (7)
#define SCU_TX_PIN_                     (1)
#define SCU_TX_FUNC_                    (FUNC6)
#define SCU_RX_PORT_                    (7)
#define SCU_RX_PIN_                     (2)
#define SCU_RX_FUNC_                    (FUNC6)

#define LPC_GPDMA_                      (LPC_GPDMA)
#define GPDMA_CONN_UART_TX_             (GPDMA_CONN_UART2_Tx)
#define GPDMA_CONN_UART_RX_             (GPDMA_CONN_UART2_Rx)

/********************** internal data declaration ****************************/

//static uint8_t rx_buffer_[RX_BUFFER_SIZE_];
//static uint8_t tx_buffer_a_[TX_BUFFER_SIZE_];
//static uint8_t tx_buffer_b_[TX_BUFFER_SIZE_];
//static uart_dma_driver_t driver_;

//static uint8_t tx_buffer_[2][TX_BUFFER_SIZE_];
//static size_t tx_size_[2];
//static int tx_index_;
//static bool tx_flag_free_;

//static uint8_t rx_buffer_[2][RX_BUFFER_SIZE_];
//static size_t rx_size_[2];
//static int rx_index_;
static uint8_t tx_dma_channel;

/********************** internal functions declaration ***********************/

static bool device_init_(void);

static bool device_driver_init_(uart_dma_driver_t *driver);

static bool channel_send_(uart_dma_driver_t *self, uart_dma_driver_tx_channel_t *channel);

static uart_dma_driver_tx_channel_t *current_channel_(uart_dma_driver_t *self);

static uint16_t current_channel_remaining_size_(uart_dma_driver_t *self);

static bool current_channel_is_full_(uart_dma_driver_t *self);

static bool current_channel_is_empty_(uart_dma_driver_t *self);

static bool current_channel_is_free_(uart_dma_driver_t *self);

static bool current_channel_is_busy_(uart_dma_driver_t *self);

static uint16_t current_channel_copy_buffer_(uart_dma_driver_t *self, uint8_t const *const buffer, uint16_t size);

static void current_channel_send_(uart_dma_driver_t *self);

static void channel_swap_(uart_dma_driver_t *self);

static void uart_dma_driver_rx_user_cb_empty_(uint8_t const *const buffer, uint16_t size);

/********************** internal data definition *****************************/

struct
{
    uart_dma_driver_t *drivers[UART_DMA_DRIVER_CONFIG_DEVICES];
    size_t drivers_size;
} device_;

/********************** external data definition *****************************/

/********************** internal functions definition ************************/

static bool device_init_(void)
{
  static bool already_initialized = false;
  if(true == already_initialized)
  {
    return;
  }
  already_initialized = true;

  for(int i = 0; i < UART_DMA_DRIVER_CONFIG_DEVICES; ++i)
  {
    device_.drivers[i] = NULL;
  }
  device_.drivers_size = 0;
}

static bool device_driver_init_(uart_dma_driver_t *driver)
{
  while(UART_DMA_DRIVER_CONFIG_DEVICES <= device_.drivers_size);

  device_.drivers[device_.drivers_size] = driver;
  device_.drivers_size++;
}

/*
 * TX
 */

static bool channel_send_(uart_dma_driver_t *self, uart_dma_driver_tx_channel_t *channel)
{
  if(NULL != self->tx.transfer_channel)
  {
     return;
  }

  self->tx.transfer_channel = channel;
  self->tx.transfer_channel->flag_free = false;
//  self->tx.transfer_channel->flag_timeout = ;

  Chip_GPDMA_Transfer(LPC_GPDMA_, tx_dma_channel, (uint32_t)&(self->tx.transfer_channel->buffer), GPDMA_CONN_UART_TX_, GPDMA_TRANSFERTYPE_M2P_CONTROLLER_DMA, self->tx.transfer_channel->size);

//  HAL_StatusTypeDef tx_status;
//  tx_status = HAL_UART_Transmit_DMA(self->uart,
//                                    self->tx.transfer_channel->buffer,
//                                    self->tx.transfer_channel->size);
//  if (HAL_OK == tx_status)
//  {
    self->tx.flag_timeout = false;
    self->tx.ticks = 0;
//  }
//  else
//  {
//    // error
//  }
}

static uart_dma_driver_tx_channel_t *current_channel_(uart_dma_driver_t *self)
{
  return self->tx.primary_channel;
}

static uint16_t current_channel_remaining_size_(uart_dma_driver_t *self)
{
  uart_dma_driver_tx_channel_t *channel = current_channel_(self);
  return self->tx.buffer_size - channel->size;
}

static bool current_channel_is_full_(uart_dma_driver_t *self)
{
  uart_dma_driver_tx_channel_t *channel = current_channel_(self);
  uint16_t remaining_size = current_channel_remaining_size_(self);
  return (0 == remaining_size);
}

static bool current_channel_is_empty_(uart_dma_driver_t *self)
{
  uart_dma_driver_tx_channel_t *channel = current_channel_(self);
  return (0 == channel->size);
}

static bool current_channel_is_free_(uart_dma_driver_t *self)
{
  uart_dma_driver_tx_channel_t *channel = current_channel_(self);
  return channel->flag_free;
}

static bool current_channel_is_busy_(uart_dma_driver_t *self)
{
  return !current_channel_is_free_(self);
}

static uint16_t current_channel_copy_buffer_(uart_dma_driver_t *self, uint8_t const *const buffer, uint16_t size)
{
  uart_dma_driver_tx_channel_t *channel = current_channel_(self);
  uint16_t remaining_size = current_channel_remaining_size_(self);
  uint16_t size_to_copy = (size <= remaining_size) ? size : remaining_size;

  memcpy((channel->buffer + channel->size), buffer, size_to_copy);
  channel->size += size_to_copy;
  return size_to_copy;
}

static void current_channel_send_(uart_dma_driver_t *self)
{
  channel_send_(self, self->tx.primary_channel);
}

static void channel_swap_(uart_dma_driver_t *self)
{
  uart_dma_driver_tx_channel_t *channel;
  channel = self->tx.primary_channel;
  self->tx.primary_channel = self->tx.secondary_channel;
  self->tx.secondary_channel = channel;
}

//static void driver_tx_isr_(uart_dma_driver_t *self, UART_HandleTypeDef *huart)
static void driver_tx_isr_(uart_dma_driver_t *self)
{
//  if (huart->Instance != self->uart->Instance)
//  {
//    return;
//  }

  if (SUCCESS == Chip_GPDMA_Interrupt(LPC_GPDMA_, tx_dma_channel))
      {
//        tx_flag_free_ = true;
  //      size_t size;
  //      tx_index_ = (0 == tx_index_) ? 1 : 0;
  //      size = RingBuffer_PopMult(&user_ring_buffer_, tx_buffer_[tx_index_], TX_BUFFER_SIZE_);
  //      Chip_GPDMA_Transfer(LPC_GPDMA_, tx_dma_channel, (uint32_t)&tx_buffer_[tx_index_], GPDMA_CONN_UART_TX_, GPDMA_TRANSFERTYPE_M2P_CONTROLLER_DMA, size);
  self->tx.transfer_channel->flag_free = true;
  self->tx.transfer_channel->size = 0;
  self->tx.transfer_channel = NULL;

  if(0 < self->tx.secondary_channel->size)
  {
    channel_send_(self, self->tx.secondary_channel);
  }
      }
      else
      {
        // error !!!
      }

}

/*
 * RX
 */

//static void uart_dma_driver_rx_user_cb_empty_(uint8_t const *const buffer, uint16_t size)
//{
//  return;
//}

//static void driver_rx_isr_(uart_dma_driver_t *self, UART_HandleTypeDef *huart, uint16_t size)
static void driver_rx_isr_(uart_dma_driver_t *self)
{
  Chip_UART_RXIntHandlerRB(LPC_UART_, &self->rx.ring_buffer);

//    if(0 == RingBuffer_IsEmpty(&user_ring_buffer_))
//    {
//      self->rx.user_cb(&self->rx.ring_buffer_);
////      if(true == tx_flag_free_)
////      {
////        tx_flag_free_ = false;
////        size_t size = RingBuffer_PopMult(&user_ring_buffer_, tx_buffer_[tx_index_], TX_BUFFER_SIZE_);
////        Chip_GPDMA_Transfer(LPC_GPDMA_, tx_dma_channel, (uint32_t)&tx_buffer_[tx_index_], GPDMA_CONN_UART_TX_, GPDMA_TRANSFERTYPE_M2P_CONTROLLER_DMA, size);
////        tx_index_ = (0 == tx_index_) ? 1 : 0;
////      }
//    }


//  if (huart->Instance != self->uart->Instance)
//  {
//    return;
//  }
//
//  uint32_t len;
//
//  if (size != self->rx.last_size)
//  {
//    if (size > self->rx.last_size)
//    {
//      len = size - self->rx.last_size;
//      self->rx.user_cb(self->rx.buffer + self->rx.last_size, len);
//    }
//    else
//    {
//      len = self->rx.buffer_size - self->rx.last_size;
//      self->rx.user_cb(self->rx.buffer + self->rx.last_size, len);
//
//      if (0 < size)
//      {
//        len = size;
//        self->rx.user_cb(self->rx.buffer, len);
//      }
//    }
//  }
//  self->rx.last_size = size;
}

/*
 * SysTick
 */

static void driver_systick_isr_(uart_dma_driver_t *self)
{
  if(false == current_channel_is_empty_(self))
  {
    self->tx.ticks++;
    if(self->tx.timeout_ticks <= self->tx.ticks)
    {
      self->tx.ticks = self->tx.timeout_ticks;
      self->tx.flag_timeout = true;

      if(false == self->tx.systick_mutex)
      {
        if(NULL == self->tx.transfer_channel)
        {
          current_channel_send_(self);
          channel_swap_(self);
        }
      }
    }
  }
}

/********************** external functions definition ************************/

void LPC_UART_IRQHANDLER_(void)
{
//  uart_dma_driver_rx_isr();
  for(int i = 0; i < device_.drivers_size; ++i)
  {
//    driver_rx_isr_(device_.drivers[i], huart, size);
    driver_rx_isr_(device_.drivers[i]);
  }
}

size_t uart_dma_driver_recibe(uart_dma_driver_t *self, uint8_t const *buffer, uint16_t size)
{
  if(0 == RingBuffer_IsEmpty(&self->rx.ring_buffer))
  {
    size = RingBuffer_PopMult(&self->rx.ring_buffer, buffer, size);
  }
  else
  {
    size = 0;
  }
  return size;
}

void DMA_IRQHandler(void)
{
  for(int i = 0; i < device_.drivers_size; ++i)
  {
//    driver_tx_isr_(device_.drivers[i], huart);
    driver_tx_isr_(device_.drivers[i]);
  }
//  // RX
//  {
//    if (SUCCESS == Chip_GPDMA_Interrupt(LPC_GPDMA_, rx_dma_channel))
//    {
//      int old_index = rx_index_;
//      rx_index_ = (0 == rx_index_) ? 1 : 0;
//      Chip_GPDMA_Transfer(LPC_GPDMA_, rx_dma_channel, GPDMA_CONN_UART_RX_, (uint32_t)&rx_buffer_[rx_index_], GPDMA_TRANSFERTYPE_P2M_CONTROLLER_DMA, RX_BUFFER_SIZE_);
//      RingBuffer_InsertMult(&user_ring_buffer_, rx_buffer_[old_index], RX_BUFFER_SIZE_);
//    }
//    else
//    {
//      // error !!!
//    }
//  }
//
//  // TX
//  uart_dma_driver_tx_isr();
//  {
//    if (SUCCESS == Chip_GPDMA_Interrupt(LPC_GPDMA_, tx_dma_channel))
//    {
//      tx_flag_free_ = true;
////      size_t size;
////      tx_index_ = (0 == tx_index_) ? 1 : 0;
////      size = RingBuffer_PopMult(&user_ring_buffer_, tx_buffer_[tx_index_], TX_BUFFER_SIZE_);
////      Chip_GPDMA_Transfer(LPC_GPDMA_, tx_dma_channel, (uint32_t)&tx_buffer_[tx_index_], GPDMA_CONN_UART_TX_, GPDMA_TRANSFERTYPE_M2P_CONTROLLER_DMA, size);
//    }
//    else
//    {
//      // error !!!
//    }
//  }
}

//void uart_dma_driver_tx_isr(UART_HandleTypeDef *huart)
//{
//  for(int i = 0; i < device_.drivers_size; ++i)
//  {
//    driver_tx_isr_(device_.drivers[i], huart);
//  }
//}

//void uart_dma_driver_rx_isr(UART_HandleTypeDef *huart, uint16_t size)
//{
//  for(int i = 0; i < device_.drivers_size; ++i)
//  {
//    driver_rx_isr_(device_.drivers[i], huart, size);
//  }
//}

void uart_dma_driver_systick_isr(void)
{
  for(int i = 0; i < device_.drivers_size; ++i)
  {
    driver_systick_isr_(device_.drivers[i]);
  }
}

uint16_t uart_dma_driver_send(uart_dma_driver_t *self, uint8_t const *buffer, uint16_t size)
{
  uint16_t size_copied = 0;

  // Primary channel
  {
    self->tx.systick_mutex = true;
    {
      if (current_channel_is_busy_(self))
      {
        // i cant copy nothing
        self->tx.systick_mutex = false;
        return size_copied;
      }
      // else is free
      size_copied += current_channel_copy_buffer_(self, buffer, size);
      if (false == current_channel_is_full_(self))
      {
        if(true == self->tx.flag_timeout)
        {
          current_channel_send_(self);
          channel_swap_(self);
        }
        self->tx.systick_mutex = false;
        return size_copied;
      }
      // else is full
      current_channel_send_(self);
      channel_swap_(self);
    }
    self->tx.systick_mutex = false;
  }

  // Update buffer and size
  {
    buffer = buffer + size_copied;
    size -= size_copied;
  }

  // Secondary channel
  {
    if (current_channel_is_busy_(self))
    {
      // i cant cpy nothing
      return size_copied;
    }
    // else is free
    size_copied += current_channel_copy_buffer_(self, buffer, size);
    if (false == current_channel_is_full_(self))
    {
      return size_copied;
    }
    // else is full
    current_channel_send_(self);
    channel_swap_(self);
  }

  return size_copied;
}

//void uart_dma_driver_receive_start(uart_dma_driver_t *self, uart_dma_driver_rx_user_cb_t user_cb)
//{
////  if (NULL == user_cb)
////  {
////    user_cb = uart_dma_driver_rx_user_cb_empty_;
////  }
////  self->rx.user_cb = user_cb;
//
////  HAL_StatusTypeDef rx_status;
////  rx_status = HAL_UARTEx_ReceiveToIdle_DMA(self->uart, self->rx.buffer, self->rx.buffer_size);
////  if (HAL_OK != rx_status)
////  {
////    // error
////  }
//}

void uart_dma_driver_init(uart_dma_driver_t *self, uart_dma_driver_config_t *config)
{
  device_init_();

  self->uart = config->uart;

  // RX
  {
    self->rx.buffer = config->rx.buffer;
    self->rx.buffer_size = config->rx.buffer_size;

//    RingBuffer_Init(&self->rx.ring_buffer_, self->rx.buffer, 1, self->rx.buffer_size);

//    self->rx.user_cb = config->rx.user_cb;
    self->rx.last_size = 0;
  }

  // TX
  {
    self->tx.buffer_size = config->tx.buffer_size;

    self->tx.channels[0].buffer = config->tx.buffer_a;
    self->tx.channels[0].flag_free = true;
    self->tx.channels[0].size = 0;

    self->tx.channels[1].buffer = config->tx.buffer_b;
    self->tx.channels[1].flag_free = true;
    self->tx.channels[1].size = 0;

    self->tx.primary_channel = &(self->tx.channels[0]);
    self->tx.secondary_channel = &(self->tx.channels[1]);
    self->tx.transfer_channel = NULL;

    self->tx.timeout_ticks = config->tx.timeout_ticks;
    self->tx.systick_mutex = false;
  }

  // periph
  {
    if(UART_DMA_DRIVER_PERIPH_UART_2 != self->uart)
    {
      while(true)
      {
        // this example only work with the UART 2 !!!
      }
    }

    Chip_UART_Init(LPC_UART_);
    Chip_UART_SetBaud(LPC_UART_, 115200);
    Chip_UART_ConfigData(LPC_UART_, (UART_LCR_WLEN8 | UART_LCR_SBS_1BIT));
    Chip_UART_SetupFIFOS(LPC_UART_, UART_FCR_FIFO_EN | UART_FCR_RX_RS | UART_FCR_TX_RS | UART_FCR_DMAMODE_SEL | UART_FCR_TRG_LEV0);

    Chip_UART_TXEnable(LPC_UART_);

    Chip_SCU_PinMux(SCU_TX_PORT_, SCU_TX_PIN_, MD_PDN, SCU_TX_FUNC_);
    Chip_SCU_PinMux(SCU_RX_PORT_, SCU_RX_PIN_, MD_PLN | MD_EZI | MD_ZI, SCU_RX_FUNC_);

//    RingBuffer_Init(&user_ring_buffer_, user_buffer_, 1, USER_BUFFER_SIZE_);
    RingBuffer_Init(&self->rx.ring_buffer, self->rx.buffer, 1, self->rx.buffer_size);

    Chip_UART_IntEnable(LPC_UART_, (UART_IER_RBRINT | UART_IER_RLSINT));

    NVIC_SetPriority(LPC_UART_IRQ_, 1);
    NVIC_EnableIRQ(LPC_UART_IRQ_);

    Chip_GPDMA_Init(LPC_GPDMA_);
    NVIC_DisableIRQ(DMA_IRQn);
    NVIC_SetPriority(DMA_IRQn, ((0x01 << 3) | 0x01));
    NVIC_EnableIRQ(DMA_IRQn);

//    rx_dma_channel = Chip_GPDMA_GetFreeChannel(LPC_GPDMA_, GPDMA_CONN_UART_RX_);
//    Chip_GPDMA_Transfer(LPC_GPDMA_, rx_dma_channel, GPDMA_CONN_UART_RX_, (uint32_t)&rx_buffer_[rx_index_], GPDMA_TRANSFERTYPE_P2M_CONTROLLER_DMA, RX_BUFFER_SIZE_);

    tx_dma_channel = Chip_GPDMA_GetFreeChannel(LPC_GPDMA_, GPDMA_CONN_UART_TX_);
//    tx_flag_free_ = true;
  }

  device_driver_init_(self);
}

/********************** end of file ******************************************/
