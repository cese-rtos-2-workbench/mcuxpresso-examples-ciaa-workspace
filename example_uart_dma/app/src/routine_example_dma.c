/*
 * Copyright (c) 2022 SEBASTIAN BEDIN <sebabedin@gmail.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * @file   : routine_example_polling.c
 * @date   : May 18, 2022
 * @author : SEBASTIAN BEDIN <sebabedin@gmail.com>
 * @version	v1.0.0
 */

/********************** inclusions *******************************************/

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

#include <arch.h>
#include <config.h>

/********************** macros and definitions *******************************/

#define LPC_UART_                       (LPC_USART2)
#define LPC_UART_IRQ_                   (USART2_IRQn)
#define LPC_UART_IRQHANDLER_            (UART2_IRQHandler)
#define SCU_TX_PORT_                    (7)
#define SCU_TX_PIN_                     (1)
#define SCU_TX_FUNC_                    (FUNC6)
#define SCU_RX_PORT_                    (7)
#define SCU_RX_PIN_                     (2)
#define SCU_RX_FUNC_                    (FUNC6)

#define LPC_GPDMA_                       (LPC_GPDMA)
#define GPDMA_CONN_UART_TX_              (GPDMA_CONN_UART2_Tx)
#define GPDMA_CONN_UART_RX_              (GPDMA_CONN_UART2_Rx)

#define PERIOD_ticks_                   (100 / APP_CONFIG_PERIOD_ms)
#define DELAY_ticks_                    (500 / APP_CONFIG_PERIOD_ms)

#define TX_BUFFER_SIZE_                 (64)
#define RX_BUFFER_SIZE_                 (1)
#define USER_BUFFER_SIZE_               (128)

/********************** internal data declaration ****************************/

/********************** internal functions declaration ***********************/

/********************** internal data definition *****************************/

static uint8_t tx_buffer_[2][TX_BUFFER_SIZE_];
//static size_t tx_size_[2];
static int tx_index_;
static bool tx_flag_free_;

static uint8_t rx_buffer_[2][RX_BUFFER_SIZE_];
//static size_t rx_size_[2];
static int rx_index_;

static RINGBUFF_T user_ring_buffer_;
static uint8_t user_buffer_[USER_BUFFER_SIZE_];

static uint8_t tx_dma_channel;
static uint8_t rx_dma_channel;

/********************** external data definition *****************************/

/********************** internal functions definition ************************/

//void LPC_UART_IRQHANDLER_(void)
//{
//  // RX
//  {
//    uint8_t byte;
//    while (Chip_UART_ReadLineStatus(LPC_UART_) & UART_LSR_RDR)
//    {
//      byte = Chip_UART_ReadByte(LPC_UART_);
//      RingBuffer_Insert(&user_ring_buffer_, &byte);
//    }
//  }
//
//  // TX
//  {
//    uint8_t byte;
//    while ((Chip_UART_ReadLineStatus(LPC_UART_) & UART_LSR_THRE) != 0 && RingBuffer_Pop(&user_ring_buffer_, &byte))
//    {
//      Chip_UART_SendByte(LPC_UART_, byte);
//    }
//  }
//}

void DMA_IRQHandler(void)
{
  // RX
  {
    if (SUCCESS == Chip_GPDMA_Interrupt(LPC_GPDMA_, rx_dma_channel))
    {
      int old_index = rx_index_;
      rx_index_ = (0 == rx_index_) ? 1 : 0;
      Chip_GPDMA_Transfer(LPC_GPDMA_, rx_dma_channel, GPDMA_CONN_UART_RX_, (uint32_t)&rx_buffer_[rx_index_], GPDMA_TRANSFERTYPE_P2M_CONTROLLER_DMA, RX_BUFFER_SIZE_);
      RingBuffer_InsertMult(&user_ring_buffer_, rx_buffer_[old_index], RX_BUFFER_SIZE_);
    }
    else
    {
      // error !!!
    }
  }

  // TX
  {
    if (SUCCESS == Chip_GPDMA_Interrupt(LPC_GPDMA_, tx_dma_channel))
    {
      tx_flag_free_ = true;
//      size_t size;
//      tx_index_ = (0 == tx_index_) ? 1 : 0;
//      size = RingBuffer_PopMult(&user_ring_buffer_, tx_buffer_[tx_index_], TX_BUFFER_SIZE_);
//      Chip_GPDMA_Transfer(LPC_GPDMA_, tx_dma_channel, (uint32_t)&tx_buffer_[tx_index_], GPDMA_CONN_UART_TX_, GPDMA_TRANSFERTYPE_M2P_CONTROLLER_DMA, size);
    }
    else
    {
      // error !!!
    }
  }
}


static void init_(void *parameters)
{
  Chip_UART_Init(LPC_UART_);
  Chip_UART_SetBaud(LPC_UART_, 115200);
  Chip_UART_ConfigData(LPC_UART_, (UART_LCR_WLEN8 | UART_LCR_SBS_1BIT));
  Chip_UART_SetupFIFOS(LPC_UART_, UART_FCR_FIFO_EN | UART_FCR_RX_RS | UART_FCR_TX_RS | UART_FCR_DMAMODE_SEL | UART_FCR_TRG_LEV0);

  Chip_UART_TXEnable(LPC_UART_);

  Chip_SCU_PinMux(SCU_TX_PORT_, SCU_TX_PIN_, MD_PDN, SCU_TX_FUNC_);
  Chip_SCU_PinMux(SCU_RX_PORT_, SCU_RX_PIN_, MD_PLN | MD_EZI | MD_ZI, SCU_RX_FUNC_);

  RingBuffer_Init(&user_ring_buffer_, user_buffer_, 1, USER_BUFFER_SIZE_);

//  Chip_UART_IntEnable(LPC_UART_, (UART_IER_RBRINT | UART_IER_RLSINT | UART_IER_THREINT));

//  NVIC_SetPriority(LPC_UART_IRQ_, 1);
//  NVIC_EnableIRQ(LPC_UART_IRQ_);

  Chip_GPDMA_Init(LPC_GPDMA_);
  NVIC_DisableIRQ(DMA_IRQn);
  NVIC_SetPriority(DMA_IRQn, ((0x01 << 3) | 0x01));
  NVIC_EnableIRQ(DMA_IRQn);

  rx_dma_channel = Chip_GPDMA_GetFreeChannel(LPC_GPDMA_, GPDMA_CONN_UART_RX_);
  Chip_GPDMA_Transfer(LPC_GPDMA_, rx_dma_channel, GPDMA_CONN_UART_RX_, (uint32_t)&rx_buffer_[rx_index_], GPDMA_TRANSFERTYPE_P2M_CONTROLLER_DMA, RX_BUFFER_SIZE_);

  tx_dma_channel = Chip_GPDMA_GetFreeChannel(LPC_GPDMA_, GPDMA_CONN_UART_TX_);
  tx_flag_free_ = true;
}

static void loop_(void *parameters)
{
  if(0 == RingBuffer_IsEmpty(&user_ring_buffer_))
  {
    if(true == tx_flag_free_)
    {
      tx_flag_free_ = false;
      size_t size = RingBuffer_PopMult(&user_ring_buffer_, tx_buffer_[tx_index_], TX_BUFFER_SIZE_);
      Chip_GPDMA_Transfer(LPC_GPDMA_, tx_dma_channel, (uint32_t)&tx_buffer_[tx_index_], GPDMA_CONN_UART_TX_, GPDMA_TRANSFERTYPE_M2P_CONTROLLER_DMA, size);
      tx_index_ = (0 == tx_index_) ? 1 : 0;
    }
  }
  return;
}

/********************** external functions definition ************************/

void routine_example_dma(void *parameters)
{
    static bool _init = true;
    static uint32_t ticks = DELAY_ticks_;
    if(_init)
    {
      _init = false;
      init_(parameters);
    }

    if(0 == ticks)
    {
      ticks = (0 < PERIOD_ticks_) ? PERIOD_ticks_ : 1;
      loop_(parameters);
    }
    ticks--;
}

/********************** end of file ******************************************/
