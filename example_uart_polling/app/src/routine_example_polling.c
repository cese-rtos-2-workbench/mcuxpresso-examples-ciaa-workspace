/*
 * Copyright (c) 2022 SEBASTIAN BEDIN <sebabedin@gmail.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * @file   : routine_example_polling.c
 * @date   : May 18, 2022
 * @author : SEBASTIAN BEDIN <sebabedin@gmail.com>
 * @version	v1.0.0
 */

/********************** inclusions *******************************************/

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

#include <arch.h>
#include <config.h>

/********************** macros and definitions *******************************/

#define LPC_UART_                       (LPC_USART2)
#define SCU_TX_PORT_                    (7)
#define SCU_TX_PIN_                     (1)
#define SCU_TX_FUNC_                    (FUNC6)
#define SCU_RX_PORT_                    (7)
#define SCU_RX_PIN_                     (2)
#define SCU_RX_FUNC_                    (FUNC6)
//#define LPC_UART_IRQ_                   (USART2_IRQn)

#define PERIOD_ticks_                   (100 / APP_CONFIG_PERIOD_ms)
#define DELAY_ticks_                    (500 / APP_CONFIG_PERIOD_ms)

#define BUFFER_SIZE_                    (64)
/*
 * 115200 / (10 * 1000) = 11.52 bytes/ms = aprox 16 = 2^4
 * 64 / 16 = 4
 */
#define TIMEOUT_MIN_ms_                 (BUFFER_SIZE_ / 16)

/********************** internal data declaration ****************************/

/********************** internal functions declaration ***********************/

/********************** internal data definition *****************************/

static uint8_t buffer_[BUFFER_SIZE_];

/********************** external data definition *****************************/

/********************** internal functions definition ************************/



static void init_(void *parameters)
{
  Chip_UART_Init(LPC_UART_);
  Chip_UART_SetBaud(LPC_UART_, 115200);
  Chip_UART_SetupFIFOS(LPC_UART_, UART_FCR_FIFO_EN | UART_FCR_TX_RS | UART_FCR_RX_RS | UART_FCR_TRG_LEV0 );
  Chip_UART_ReadByte(LPC_UART_);
  Chip_UART_TXEnable(LPC_UART_);
  Chip_SCU_PinMux(SCU_TX_PORT_, SCU_TX_PIN_, MD_PDN, SCU_TX_FUNC_);
  Chip_SCU_PinMux(SCU_RX_PORT_, SCU_RX_PIN_, MD_PLN | MD_EZI | MD_ZI, SCU_RX_FUNC_);
}

static void loop_(void *parameters)
{
  uint16_t rx_len;

  // RX
  {
    rx_len = Chip_UART_Read(LPC_UART_, (void*)buffer_, BUFFER_SIZE_);
  }

  // TX
  {
    if(0 < rx_len)
    {
      Chip_UART_SendBlocking(LPC_UART_, buffer_, rx_len);
    }
  }
}

/********************** external functions definition ************************/

void routine_example_polling(void *parameters)
{
    static bool _init = true;
    static uint32_t ticks = DELAY_ticks_;
    if(_init)
    {
      _init = false;
      init_(parameters);
    }

    if(0 == ticks)
    {
      ticks = (0 < PERIOD_ticks_) ? PERIOD_ticks_ : 1;
      loop_(parameters);
    }
    ticks--;
}

/********************** end of file ******************************************/
